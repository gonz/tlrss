#lang setup/infotab

(define name "tlrss")

(define blurb
  "A library and accompanying application to automatically download torrents from TL")

(define scribblings '(("tlrss-manual.scrbl" ())))
